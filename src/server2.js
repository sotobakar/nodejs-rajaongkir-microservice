const express = require("express");
const axios = require("axios");
const app2 = express();

const API_KEY = "8f8998d694db30a37fb3066b57e07835";

const instance = axios.create({
  baseURL: "https://api.rajaongkir.com/starter",
  headers: {
    key: API_KEY,
  },
});

app2.use(express.json()); // for parsing application/json

app2.get("/provinsi", async (req, res) => {
  try {
    const response = await instance.get("/province");
    const data = response.data.rajaongkir.results;
    return res.json(data);
  } catch (error) {
    return res.json(
      {
        message: "Pengambilan data provinsi gagal",
      },
      400
    );
  }
});

app2.get("/kota", async (req, res) => {
  try {
    const response = await instance.get("/city");
    const data = response.data.rajaongkir.results;
    return res.json(data);
  } catch (error) {
    return res.json(
      {
        message: "Pengambilan data kota gagal",
      },
      400
    );
  }
});

app2.post("/hitung", async (req, res) => {
  const body = req.body;
  try {
    const response = await instance.post("/cost", {
      origin: body.asal,
      destination: body.tujuan,
      weight: body.berat,
      courier: body.kurir,
    });

    const data = response.data.rajaongkir.results;
    return res.json(data);
  } catch (error) {
    console.log(error);
    return res.status(400).json({
      message: error,
    });
  }
});

app2.listen(3002, () => {
  console.log(`Server 2 listen on port 3002`);
});

