const express = require("express");
const sqlite3 = require("sqlite3").verbose();
const app1 = express();

let db = new sqlite3.Database(':memory:')

db.serialize(() => {
  db.run("CREATE TABLE ongkir (kota_asal TEXT, kota_tujuan TEXT, berat TEXT, kurir TEXT, tarif TEXT)")

  db.run("INSERT INTO ongkir VALUES (?, ?, ?, ?, ?)", "100", "115", "1000", "jne", "12000")
})

app1.use(express.json()); // for parsing application/json

// TODO: Simpan ongkir ke mini database/sqlite
app1.post("/save_ongkir", async (req, res) => {
  const { asal, tujuan, berat, kurir, tarif } = req.body
  db.serialize(() => {
    db.run("INSERT INTO ongkir VALUES (?, ?, ?, ?, ?)", asal, tujuan, berat, kurir, tarif)

  })

  return res.json({
    message: "Data Ongkir disimpan",
    data: req.body,
  });
});

// TODO: Get Ongkir yang disimpan
app1.get("/daftar_ongkir", async (req, res) => {
  db.serialize(() => {
    db.all("SELECT * FROM ongkir", function(err, rows = []) {
      if (err) {
        return res.status(400).json({
          message: "Gagal mengambil data ongkir"
        })
      }
      return res.json({
        message: "Data-data ongkir yang disimpan pada database",
        data: rows,
      })
    })
  })
  
});

app1.listen(3001, () => {
  console.log(`Server 1 listen on port 3001`);
});