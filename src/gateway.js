const express = require("express");
const axios = require("axios");
const app3 = express();

const server2 = axios.create({
  baseURL: "http://localhost:3002",
});

const server1 = axios.create({
  baseURL: "http://localhost:3001",
});

app3.use(express.json()); // for parsing application/json

// TODO: Gateway route untuk menghitung dan menyimpan data ongkir
app3.post("/hitung_simpan_ongkir", async (req, res) => {
  const body = req.body;
  try {
    const responseCalculate = await server2.post("/hitung", body);
    const fee = responseCalculate.data[0].costs[0].cost[0].value 
    const responseSave = await server1.post('/save_ongkir', {
      ...body,
      tarif: fee
    })

    return res.json({
      message: 'Data ongkir berhasil disimpan',
      data: responseSave.data.data
    })
  } catch (error) {
    // console.log(error)
    return res.status(400).json({
      "message": "Test"
    })
  }

});


app3.listen(3003, () => {
  console.log(`Gateway Server listen on port 3003`);
});
