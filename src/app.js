const express = require("express");
const axios = require("axios");
const sqlite3 = require("sqlite3").verbose();
const app1 = express();
const app2 = express();
const app3 = express();

let db = new sqlite3.Database(':memory:')

db.serialize(() => {
  db.run("CREATE TABLE ongkir (kota_asal TEXT, kota_tujuan TEXT, berat TEXT, kurir TEXT, tarif TEXT)")

  db.run("INSERT INTO ongkir VALUES (?, ?, ?, ?, ?)", "100", "115", "1000", "jne", "12000")
})

const API_KEY = "8f8998d694db30a37fb3066b57e07835";

const instance = axios.create({
  baseURL: "https://api.rajaongkir.com/starter",
  headers: {
    key: API_KEY,
  },
});

const server2 = axios.create({
  baseURL: "http://localhost:3002",
});

const server1 = axios.create({
  baseURL: "http://localhost:3001",
});

app1.use(express.json()); // for parsing application/json
app2.use(express.json()); // for parsing application/json
app3.use(express.json()); // for parsing application/json

app2.get("/provinsi", async (req, res) => {
  try {
    const response = await instance.get("/province");
    const data = response.data.rajaongkir.results;
    return res.json(data);
  } catch (error) {
    return res.json(
      {
        message: "Pengambilan data provinsi gagal",
      },
      400
    );
  }
});

app2.get("/kota", async (req, res) => {
  try {
    const response = await instance.get("/city");
    const data = response.data.rajaongkir.results;
    return res.json(data);
  } catch (error) {
    return res.json(
      {
        message: "Pengambilan data kota gagal",
      },
      400
    );
  }
});

app2.post("/hitung", async (req, res) => {
  const body = req.body;
  try {
    const response = await instance.post("/cost", {
      origin: body.asal,
      destination: body.tujuan,
      weight: body.berat,
      courier: body.kurir,
    });

    const data = response.data.rajaongkir.results;
    return res.json(data);
  } catch (error) {
    console.log(error);
    return res.status(400).json({
      message: error,
    });
  }
});

// TODO: Simpan ongkir ke mini database/sqlite
app1.post("/save_ongkir", async (req, res) => {
  const { asal, tujuan, berat, kurir, tarif } = req.body
  db.serialize(() => {
    db.run("INSERT INTO ongkir VALUES (?, ?, ?, ?, ?)", asal, tujuan, berat, kurir, tarif)

  })

  return res.json({
    message: "Data Ongkir disimpan",
    data: req.body,
  });
});

// TODO: Get Ongkir yang disimpan
app1.get("/daftar_ongkir", async (req, res) => {
  db.serialize(() => {
    db.all("SELECT * FROM ongkir", function(err, rows = []) {
      if (err) {
        return res.status(400).json({
          message: "Gagal mengambil data ongkir"
        })
      }
      return res.json({
        message: "Data-data ongkir yang disimpan pada database",
        data: rows,
      })
    })
  })
  
});

// TODO: Gateway route untuk menghitung dan menyimpan data ongkir
app3.post("/hitung_simpan_ongkir", async (req, res) => {
  const body = req.body;
  try {
    const responseCalculate = await server2.post("/hitung", body);
    const fee = responseCalculate.data[0].costs[0].cost[0].value 
    const responseSave = await server1.post('/save_ongkir', {
      ...body,
      tarif: fee
    })

    return res.json({
      message: 'Data ongkir berhasil disimpan',
      data: responseSave.data.data
    })
  } catch (error) {
    // console.log(error)
    return res.status(400).json({
      "message": "Test"
    })
  }

});

app1.listen(3001, () => {
  console.log(`Server 1 listen on port 3001`);
});

app2.listen(3002, () => {
  console.log(`Server 2 listen on port 3002`);
});

app3.listen(3003, () => {
  console.log(`Gateway Server listen on port 3003`);
});
